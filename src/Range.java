import java.util.*;
public class Range {
    public static void main(String args[])
    {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int arr[]=new int[40];
        for(int i=0;i<n;i++)
        {
            arr[i]=sc.nextInt();
        }
        int min=arr[0];
        int max=arr[0];
        for(int i=0;i<n;i++) {
            min = Math.min(min, arr[i]);
            max = Math.max(max, arr[i]);
        }
        int range=max-min;
        System.out.println(range);
    }
}
