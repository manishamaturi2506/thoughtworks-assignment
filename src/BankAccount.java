public class BankAccount {
        private final String nameOfTheAccountHolder;
        private int balanceInTheAccount;
        private final String typeOfAccount;

        BankAccount(String name, int balance, String type){
            nameOfTheAccountHolder = name;
            balanceInTheAccount = balance;
            typeOfAccount = type;
        }

        public int getBalanceInTheAccount(){
            return balanceInTheAccount;
        }

        public double getWithdrawableBalance(){
            String typeOfAccount = this.getTypeOfAccount();
            double balanceInAccount = this.getBalanceInTheAccount();

            if(typeOfAccount.equals("Current")){
                return (balanceInAccount + (0.2 * balanceInAccount));
            }
            else{
                return balanceInAccount;
            }
        }

        public String getNameOfTheAccountHolder(){
            return nameOfTheAccountHolder;
        }

        public String getTypeOfAccount(){
            return typeOfAccount;
        }

        public void depositMoney(int money){
            balanceInTheAccount += money;
            System.out.println("Deposited Successfully. Current balance is " + balanceInTheAccount);
        }

        public void withdrawMoney(int money){
            double balanceInAccount = this.getWithdrawableBalance();

            if(balanceInAccount >= money) {
                balanceInTheAccount -= money;
                System.out.println("Withdrawn Successfully. Current balance is " + balanceInTheAccount);
            }
            else{
                System.out.println("Insufficient Funds");
            }

        }

        public double calculateInterestFromJanuaryToMarch(){
            double rateOfInterest = 0;
            String typeOfAccount = this.getTypeOfAccount();
            double balanceInAccount = this.getBalanceInTheAccount();

            if(typeOfAccount.equals("Current")){
                rateOfInterest = 0;
            }
            else if(typeOfAccount.equals("Savings")){
                rateOfInterest = 0.03;
            }

            return (rateOfInterest * balanceInAccount) / 4;
        }

        public static void main(String[] args){
            BankAccount gopal = new BankAccount("Gopal", 50000, "Current");
            BankAccount amrita = new BankAccount("Amrita", 100000, "Savings");

            gopal.depositMoney(10000);
            amrita.withdrawMoney(45000);

            System.out.println(gopal.getWithdrawableBalance());

            System.out.println("Gopal's interest: " + gopal.calculateInterestFromJanuaryToMarch());
            System.out.println("Amrita's interest: " + amrita.calculateInterestFromJanuaryToMarch());

            gopal.withdrawMoney(70000);
        }


    }


