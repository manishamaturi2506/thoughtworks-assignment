import java.io.*;
import java.lang.*;
import java.util.*;
class StatisticalMeasures
{
    public static void main(String[] args)
    {   Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[] arr = new int[n];
        for(int i=0;i<n;i++)
        {
            arr[i]=sc.nextInt();
        }
        double tot=0;
        double mean=0.00000;
        for(int i=0; i<n; i++)
        {
            tot = tot+arr[i];
        }
        mean = tot/n;
        System.out.println(String.format("%.2f",mean));
        double median = 0.000000;
        if(n%2 == 0)
        {
            int temp=(n/2)-1;
            for(int i=0;i<n;i++)
            {
                if(temp==i || (temp+1)==i)
                {
                    median=median+arr[i];
                }
            }
            median=median/2;
            System.out.println(String.format("%.2f",median));
        }
        else
        {
            int temp=(n/2);
            for(int i=0;i<n;i++)
            {
                if(temp==i)
                {
                    median=arr[i];
                    System.out.println(String.format("%.2f",median));
                }
            }
        }
        int i,j,z, tmp, maxCount;
        double modeValue=0.00000;
        int[] mode=new int[n];
        for(i=0;i<n;i++)
        {
            for(j=0;j<n-i;j++)
            {
                if(j+1!=n)
                {
                    if(arr[j]>arr[j+1])
                    {
                        tmp=arr[j];
                        arr[j]=arr[j+1];
                        arr[j+1]=tmp;
                    }
                }
            }
        }
        for (i = 0; i < n; i++)
        {
            for(z=i+1;z<n;z++)
            {
                if(arr[i]==arr[z])
                {
                    mode[i]++;
                }
            }
        }
        maxCount = 0;
        for (i = 0; i <n; i++)
        {
            if (mode[i] > maxCount)
            {
                maxCount = mode[i];
                modeValue = arr[i];
            }
        }
        System.out.println(String.format("%.2f",modeValue));
    }
}