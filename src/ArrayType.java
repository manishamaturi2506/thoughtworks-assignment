import java.util.*;
import java.util.*;
class ArrayType {
    private static int CheckArrayType(int[] x, int n) {
        int odd = 0, even = 0;
        for (int i = 0; i < n; i++) {
            if (x[i] % 2 == 1)
                odd++;
            else
                even++;
        }
        if (odd == n)
            return  2;
        else if (even == n)
            return 1;
        else
            return 3;
    }
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        if(CheckArrayType(arr, n)==1)
        {
            System.out.println("Even");

        }
        if(CheckArrayType(arr, n)==2)
        {
            System.out.println("Odd");
        }
        if(CheckArrayType(arr, n)==3)
        {
            System.out.println("Mixed");
        }
    }
}


