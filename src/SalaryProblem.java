import java.util.*;
import java.io.*;
class SalaryProblem {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int sun, mon, tue, wed, thurs, fri, sat;
        int bonus, bonus_rate, rate1=0, rate2=0, rate3=0, rate4=0, rate5=0, rate6=0, rate7=0, rate8 = 0;
        int edays, total_days, salary;
        sun = sc.nextInt();
        mon = sc.nextInt();
        tue = sc.nextInt();
        wed = sc.nextInt();
        thurs = sc.nextInt();
        fri = sc.nextInt();
        sat = sc.nextInt();
        if(sun<=8)
        {
            rate1 = sun * 150;
        }
        else if(sun>8)
        {
            bonus=sun-8;
            bonus_rate=bonus*115;
            rate1=bonus_rate+(150*8);
        }
        if(sat<=8)
        {
            rate7 = sat * 125;
        }
        else if(sat>8)
        {
            bonus=sat-8;
            bonus_rate=bonus*115;
            rate7=bonus_rate+(125*8);
        }
        if (mon <= 8) {
            rate2 = mon * 100;
        } else if (mon > 8) {
            bonus = mon - 8;
            bonus_rate = bonus *115;
            rate2 = bonus_rate + 800;
        }
        if (tue <= 8) {
            rate3 = tue * 100;
        } else if (tue > 8) {
            bonus = tue - 8;
            bonus_rate = bonus * 115;
            rate3 = bonus_rate + 800;
        }
        if (wed <= 8) {
            rate4 = wed * 100;
        } else if (wed > 8) {
            bonus = wed - 8;
            bonus_rate = bonus * 115;
            rate4 = bonus_rate + 800;
        }
        if (thurs <= 8) {
            rate5 = thurs * 100;
        } else if (thurs > 8) {
            bonus = thurs - 8;
            bonus_rate = bonus * 115;
            rate5 = bonus_rate + 800;
        }
        if (fri <= 8) {
            rate6 = fri * 100;
        } else if (fri > 8) {
            bonus = fri - 8;
            bonus_rate = bonus * 115;
            rate6 = bonus_rate + 800;
        }
        total_days = mon + tue + wed + thurs + fri;
        if (total_days > 40) {
            edays = total_days - 40;
            rate8 = edays * 25;
        }
        salary = rate1 + rate2 + rate3 + rate4 + rate5 + rate6 + rate7 + rate8;
        System.out.println(salary);
    }
}