import java.util.*;

public class MovieChecker {
    public static void main(String[] args) {
        Movie m1= new Movie("Casino Royale", "Eon Productions", "PG-13");

        Movie m2 = new Movie("Indiana Jones", "Raiders", "PG");
        Movie m3 = new Movie("Deadpool", "Fox", "R");
        Movie m4 = new Movie("Life Of Pi", "20th Century", "PG");
        Movie[] movies = {m1, m2, m3, m4};
        Movie[] pgRatedMovies = m1.getPg(movies);
        for(Movie m: pgRatedMovies){
            System.out.println(m.getTitle());
        }

    }
}

