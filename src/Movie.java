import java.util.Arrays;

public class Movie {
    private String title;
    private String studio;
    private String rating;

    public Movie(String title, String studio, String rating) {
        this.title = title;
        this.studio = studio;
        this.rating = rating;
    }

    public Movie(String title, String studio) {
        this.title = title;
        this.studio = studio;
        this.rating = "PG";
    }

    public String getRating() {
        return this.rating;
    }

    public String getStudio() {
        return this.studio;
    }

    public String getTitle() {
        return this.title;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setStudio(String studio) {
        this.studio = studio;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Movie[] getPg(Movie[] movies) {

        Movie[] result = new Movie[movies.length];
        int k = 0;

        for (Movie m : movies) {
            if (m.getRating().contains("PG"))
                result[k++] = m;
        }

        return Arrays.copyOfRange(result, 0, k);

    }
}
