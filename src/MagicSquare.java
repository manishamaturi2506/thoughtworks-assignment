import java.io.*;
import java.util.*;
class MagicSquare{
    static boolean isMagicSquare(int mat[][]) {
        int N = mat.length;
        int K = mat.length-1;
        int reversediagonalsum = 0;
        for (int i = 0; i < N; i++)
            reversediagonalsum = reversediagonalsum + mat[i][K];
        K = K - 1;
        int diagonalsum = 0;
        for (int i = 0; i < N; i++)
            diagonalsum = diagonalsum + mat[i][i];
        if (diagonalsum != reversediagonalsum) {
            return false;
        }
        for (int i = 0; i < N; i++) {
            int rowSum = 0;
            int colSum = 0;
            for (int j = 0; j < N; j++)
                colSum += mat[j][i];
            for (int j = 0; j < N; j++)
                rowSum += mat[i][j];
            if (diagonalsum != colSum | reversediagonalsum != colSum |rowSum!=colSum)
                return false;
            if (rowSum != diagonalsum | rowSum != reversediagonalsum)
                return false;
        }
        return true;
    }
    public static void main(String[] args)
    {
        Scanner sc=new Scanner(System.in);
        int dimension=sc.nextInt();
        int matrix[][]=new int[dimension][dimension];
        for(int i=0; i<dimension;i++)
        {
            for(int j=0; j<dimension;j++)
            {
                matrix[i][j]=sc.nextInt();
            }
        }
        for(int []x:matrix){
            for(int y:x){
                System.out.print(y+"        ");
            }
            System.out.println();
        }
        if (isMagicSquare(matrix))
            System.out.println("yes");
        else
            System.out.println("no");
    }
}